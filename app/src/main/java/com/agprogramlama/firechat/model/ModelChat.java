package com.agprogramlama.firechat.model;

/**
 * Created by se on 12/26/17.
 *
 *
 * @github @agprogramlama
 */

public class ModelChat {

    private String user;
    private String message;
    private String time;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
