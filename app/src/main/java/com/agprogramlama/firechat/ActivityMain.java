package com.agprogramlama.firechat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.agprogramlama.firechat.adapter.AdapterChat;
import com.agprogramlama.firechat.model.ModelChat;
import com.agprogramlama.firechat.model.ModelUser;
import com.agprogramlama.firechat.network.ChatRequest;
import com.agprogramlama.firechat.utils.Constan;
import com.agprogramlama.firechat.utils.EditTextListener;
import com.agprogramlama.firechat.utils.PreferencesManager;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityMain extends AppCompatActivity {

    @BindView(R.id.nav_user)
    TextView navUser;

    @BindView(R.id.nav_email)
    TextView navEmail;

    @BindView(R.id.mainToolbar)
    Toolbar toolbar;

    @BindView(R.id.mainDrawer)
    DrawerLayout drawerLayout;

    @BindView(R.id.btnSend)
    Button btnSend;

    @BindView(R.id.etMessage)
    EditText etMessage;

    @BindView(R.id.chatItem)
    RecyclerView chatItem;

    @OnClick(R.id.btnSignOut)
    public void signOut() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
        builder.setTitle("Sign Out");
        builder.setMessage("Çıkış yapmak ister misin ?");
        builder.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                FirebaseAuth auth = FirebaseAuth.getInstance();
                auth.signOut();

                startActivity(new Intent(ActivityMain.this , ActivityLauncher.class));
                ActivityMain.this.finish();

            }
        });
        builder.setNegativeButton("Hayır" , null);
        builder.create().show();

        drawerLayout.closeDrawers();
        
    }

    AdapterChat adapterChat;
    List<ModelChat> listChat = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        bindUI();

        ChatRequest.getChat(new ChatRequest.OnChatRequest() {
            @Override
            public void result(ModelChat chat) {
                listChat.add(chat);

                if (listChat.size() > 100) {
                    listChat.remove(0);
                }

                adapterChat.notifyDataSetChanged();
            }
        });

    }

    private void bindUI() {

        adapterChat = new AdapterChat(listChat);

        chatItem.setHasFixedSize(true);
        chatItem.setLayoutManager(new LinearLayoutManager(this));
        chatItem.setAdapter(adapterChat);

        ModelUser user = PreferencesManager.initPreferences().getUserInfo();

        navUser.setText(user.getUsername());
        navEmail.setText(user.getEmail());
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_drawer, null);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, Color.WHITE);
        toolbar.setNavigationIcon(drawable);
        toolbar.setTitle("AgProgramlama Chat!");
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();




        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);

        etMessage.addTextChangedListener(new EditTextListener(btnSend));
        etMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

            }
        });



        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = MyApplication.getInstance().getSharedPreferences().getString(Constan.PREF_USERNAME, "user");
                String Message = etMessage.getText().toString();
                String Time = Constan.getTime();

                ModelChat chat = new ModelChat();
                chat.setUser(username);
                chat.setMessage(Message);
                chat.setTime(Time);

                ChatRequest.postMessage(chat);

                etMessage.setText("");
                MyApplication.hideSoftInput(ActivityMain.this , etMessage);
            }
        });




    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        // return true so that the menu pop up is opened
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Intent i =new Intent(ActivityMain.this,ActivitySelectFriend.class);
                startActivity(i);
                break;
            default:
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
