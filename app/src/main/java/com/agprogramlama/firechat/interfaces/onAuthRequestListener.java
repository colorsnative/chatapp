package com.agprogramlama.firechat.interfaces;

/**
 * Created by se on 9/15/17.
 *
 *
 * @github @agprogramlama
 *
 */

public interface onAuthRequestListener {

    void onAuthFailed(String Error);

    void onAuthSuccess();

}
