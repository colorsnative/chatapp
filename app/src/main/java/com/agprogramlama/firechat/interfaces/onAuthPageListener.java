package com.agprogramlama.firechat.interfaces;

/**
 * Created by se on 9/15/17.
 *
 *
 * @github @agprogramlama
 *
 */

public interface onAuthPageListener {
    void onLoginPage();

    void onRegisterPage();

    void onAuthenticateSuccess();
}
