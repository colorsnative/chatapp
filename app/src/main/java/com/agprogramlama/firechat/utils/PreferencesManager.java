package com.agprogramlama.firechat.utils;

import android.content.SharedPreferences;

import com.agprogramlama.firechat.model.ModelUser;
import com.agprogramlama.firechat.MyApplication;

/**
 * Created by se on 9/15/17.
 *
 *
 * @github @agprogramlama
 */

public class PreferencesManager {

    private static SharedPreferences preferences;

    public static PreferencesManager initPreferences() {
        preferences = MyApplication.getInstance().getSharedPreferences();
        return new PreferencesManager();
    }

    public void setUserInfo(ModelUser modelUser) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constan.PREF_USERNAME, modelUser.getUsername());
        editor.putString(Constan.PREF_EMAIL, modelUser.getEmail());
        editor.putString(Constan.PREF_USERID, modelUser.getUserId());
        editor.putString(Constan.PREF_LOGIN_TIME, modelUser.getLoginTime());
        editor.apply();
    }

    public ModelUser getUserInfo() {

        ModelUser user = new ModelUser();
        user.setUsername(preferences.getString(Constan.PREF_USERNAME, ""));
        user.setEmail(preferences.getString(Constan.PREF_EMAIL, ""));
        user.setUserId(preferences.getString(Constan.PREF_USERID, ""));
        user.setLoginTime(preferences.getString(Constan.PREF_LOGIN_TIME, ""));

        return user;
    }
}
